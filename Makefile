PWD ?= $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
BIN := "grawler"
PKG := gitlab.com/ritho/$(BIN)

.DEFAULT_GOAL := build

build-dirs:
	@mkdir -p $(PWD)/bin

clean:
	@rm -fr $(PWD)/bin
	@rm -fr $(PWD)/out

.PHONY: build
build:
	@$(MAKE) build-dirs
	@go build -o $(PWD)/bin/${BIN} $(PWD)/cmd/${BIN}/main.go

test:
	@go test -v ./... -race -coverprofile cover.out
	@go tool cover -func "cover.out"

test-update:
	@go test ./... -tags=update

lint:
	@golangci-lint run -c $(PWD)/.golangci.yaml

update-lint:
	@curl -sfL https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | sh -s -- -b $(shell go env GOPATH)/bin latest

run: build
	@./bin/grawler --url https://crawler-test.com/ --dir out --resume --debug
