# Grawler

Grawler is a recursive, mirroring web crawler developed in Go.

It currently accepts the following flags:

* --url url: It's a mandatory flag to indicate the url to mirror.
* --dir out: It's a mandatory flag to indicate the output directory where you want to store the web pages downloaded.
* --resume: It's an optional flag to indicate you want to continue with a previous download.
* --debug: It's an optional flag to see debug messages.

## Improvements

* Parallelization: The links are fetched one by one. That part can be easily parallelized, since the links model doesn't
  store duplicated links and it's thread-safe, and once a link is obtained from the links model, it's removed, so we
  can launch one goroutine per link. The storage could be done also asynchronous, but that would make more difficult the
  error handling, since if, for some reason, the content can't be stored, we would need to retry the storage part, which,
  depending on the error, could mean fetching the content again.
* Circular dependencies: Right now I'm not checking properly circular dependencies between pages, so if two pages depends
  between each other we could enter in an infinite loop. To fix that it would make sense to store the links history in
  the links model in some way and checking the history before adding a new link.
* Links model: Right now the links model is a FIFO without repetition, which, in the current implementation, goes linearly
  through the list to check if the link is already there, which, since we're not going to manage too much links, it's a
  good compromise, but to add the links history it would be better to add a map with that history, where the key could
  be the base64 string of the url, in that way we don't need to go through the whole list to search for a link.
* Signals: Right now I'm not managing the interrupt signal to stop properly the process. The easiest way to do it is
  with go channels in a similar wat of https://gitlab.com/ritho/blog/-/blob/master/cmd/blog/main.go#L48, but waiting for
  the current page to finish downloading and saving in the proper directory. In case of dealing with several page
  downloads at the same time, the safe way is having a [waiting group](https://pkg.go.dev/sync#WaitGroup) to wait for
  all the processes to finish.
* Logs: I've added a quite small package for logging, specially to allow debugging output, but it would make sense
  to improve the log package or use an external log implementation, like [glog](https://pkg.go.dev/github.com/golang/glog).
* Errors: As same as with the logs, the error management could be improved. I've made the minimum to handle the flow
  properly, but in some cases the error management could be improved, like retrying the operation in some cases or
  even stop them in other cases (for example, we could deal specially the case of a link that has too many redirections).
* Flags improvements: We could include a `force` flag for the case we want to force an update in the storage, even if
  we're resuming the download. It would be also good to add some kind of content verification (shasum and/or timestamp, for
  example), to know if the page has been updated or it's safe to ignore it.
* Content identification: Currently I've assumed everything is html, so I've put always the `.html` suffix, so an useful
  improvement can be detect the content type and set the extension depending on the content type, which with `resty`
  shouldn't be hard to check the `content-type` header.
* Tests: I've been checking the command against https://crawler-test.com/, which gives a bunch of edge cases, but
  I haven't got the time to write proper unit tests, so a clear improvement would be adding a bunch of unit tests
  using table driven tests.
* Documentation: Even if I've added some minimal comments on the code and, in general, I've tried to make the code
  clear enough, I haven't added any documentation on all my code choices, so another clear improvement is adding
  more documentation.
