package main

import (
	"flag"
	"fmt"
	"net/url"
	"os"
	"strings"

	"gitlab.com/ritho/grawler/internal/fetcher"
	"gitlab.com/ritho/grawler/internal/log"
	"gitlab.com/ritho/grawler/internal/models"
	"gitlab.com/ritho/grawler/internal/parser"
	"gitlab.com/ritho/grawler/internal/storer"
)

type params struct {
	url    string
	dir    string
	resume bool
	help   bool

	baseURL string
	path    string
	uri     string
}

func main() {
	params := parseParams()

	client := fetcher.New(params.baseURL)
	p := parser.New(params.baseURL, params.path)
	storage, err := storer.New(params.dir, params.resume)
	if err != nil {
		fmt.Printf("%s\n", err)
		os.Exit(1)
	}

	links := initializeLinks(params)
	log.Debugf("Fetching %d links\n", links.Len())
	for links.Len() > 0 {
		link, err := links.Pull()
		if err != nil {
			fmt.Printf("%s\n", err)
			os.Exit(1)
		}

		if params.resume && storage.Exists(link.String()) {
			fmt.Printf("%s already downloaded, continuing", link.String())
		}

		log.Debugf("Fetching link %s\n", link.String())
		content, err := client.Fetch(link)
		if err != nil {
			fmt.Printf("error fetching the content: %s ignoring...\n", err)
			continue
		}

		log.Debugf("Storing content for link %s\n", link.String())
		if err = storage.Store(link.String(), content, false); err != nil {
			fmt.Printf("%s\n", err)
			os.Exit(1)
		}

		log.Debugf("content stored, parsing for new links.\n")
		discoveredLinks, err := p.Parse(content)
		if err != nil {
			fmt.Printf("%s\n", err)
			os.Exit(1)
		}

		for _, newLink := range discoveredLinks.Links() {
			links.Push(newLink)
		}
	}
}

func initializeLinks(params *params) *models.Links {
	link, err := models.NewLink(params.uri)
	if err != nil {
		fmt.Printf("%s\n", err)
		os.Exit(1)
	}

	links := models.NewLinks()
	links.Push(link)

	return links
}

func parseParams() *params {
	p := new(params)

	flag.StringVar(&p.url, "url", "", "url to fetch")
	flag.StringVar(&p.dir, "dir", "", "directory to save the html pages fetched")
	flag.BoolVar(&p.resume, "resume", false, "resume a previous execution")
	flag.BoolVar(&p.help, "help", false, "print the help message")
	flag.BoolVar(&log.Debug, "debug", false, "debug mode for the command")
	flag.Parse()

	if p.help {
		showHelp()
		os.Exit(0)
	}

	if p.url == "" {
		fmt.Println("the url shouldn't be empty")
		usage()
		os.Exit(1)
	}

	if p.dir == "" {
		curDir, err := os.Getwd()
		if err != nil {
			fmt.Printf("unable to get the current directory: %s\n", err)
			os.Exit(1)
		}

		p.dir = curDir
	}

	u, err := url.Parse(p.url)
	if err != nil {
		fmt.Printf("unable to parse the url: %s\n", err)
		usage()
		os.Exit(1)
	}

	p.uri = u.RequestURI()
	p.baseURL = strings.TrimSuffix(p.url, p.uri)
	p.path = u.Path
	if p.path == "" {
		p.path = p.uri
	}

	return p
}

func usage() {
	fmt.Printf("%s --url url --dir dir [--resume]", os.Args[0])
}

func showHelp() {
	fmt.Printf("%s --url url --dir dir [--resume]", os.Args[0])
}
