// Package fetcher implements the html fetcher from the crawler.
package fetcher

import (
	"fmt"
	"strings"
	"time"

	"github.com/go-resty/resty/v2"

	"gitlab.com/ritho/grawler/internal/log"
	"gitlab.com/ritho/grawler/internal/models"
)

const (
	requestTimeout = 10 * time.Second
)

// Fetcher fetches a specific resource
type Fetcher interface {
	Fetch(link models.Link) (string, error)
}

type htmlFetcher struct {
	client  *resty.Client
	baseURL string
}

// New returns a new html fetcher.
func New(baseURL string) Fetcher {
	return &htmlFetcher{
		baseURL: baseURL,
		client: resty.New().SetBaseURL(strings.TrimSuffix(baseURL, "/")).
			SetTimeout(requestTimeout).
			SetDebug(log.Debug),
	}
}

// Fetch the page content and store it in memory to be consumed later.
func (f *htmlFetcher) Fetch(link models.Link) (string, error) {
	uri := link.String()
	log.Debugf("fetching %s from %s\n", uri, f.baseURL)
	resp, err := f.client.R().Get(uri)
	if err != nil {
		return "", err
	}

	if resp.IsError() {
		fmt.Printf("error fetching the html page at %s%s: status code %d\n",
			f.baseURL, uri, resp.StatusCode())
		return "", nil
	}

	return string(resp.Body()), nil
}
