// Package log implements a basic logger for the crawler.
package log

import (
	"fmt"
)

// Debug indicates if we need to print the debug messages.
var Debug bool // nolint:gochecknoglobals

// Debugf prints a debug message if it's enabled.
func Debugf(format string, args ...interface{}) {
	if Debug {
		fmt.Printf(format, args...)
	}
}
