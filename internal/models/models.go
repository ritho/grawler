// Package models contains the common models for the crawler.
package models

import (
	"errors"
	"net/url"
	"sync"
)

// Link represent an html link, which is basically an url.
type Link string

// NewLink creates a new link to be dealed by the crawler.
func NewLink(link string) (Link, error) {
	if _, err := url.Parse(link); err != nil {
		return "", err
	}

	return Link(link), nil
}

// String returns a link as a string.
func (l Link) String() string {
	return string(l)
}

// Links represents a list of links.
type Links struct {
	links []Link
	mutex *sync.Mutex
}

// NewLinks returns a new Links variable.
func NewLinks() *Links {
	return &Links{
		links: make([]Link, 0),
		mutex: &sync.Mutex{},
	}
}

// Contains checks if the list of links contains a specific link or not.
func (l *Links) Contains(link Link) bool {
	l.mutex.Lock()
	defer l.mutex.Unlock()
	for _, lnk := range l.links {
		if lnk == link {
			return true
		}
	}

	return false
}

// Pull the first link.
func (l *Links) Pull() (Link, error) {
	l.mutex.Lock()
	defer l.mutex.Unlock()

	if len(l.links) > 0 {
		link := l.links[0]
		l.links = l.links[1:]

		return link, nil
	}

	return "", errors.New("not enough elements")
}

// Push a new element to the
func (l *Links) Push(link Link) {
	if l.Contains(link) {
		return
	}

	l.mutex.Lock()
	defer l.mutex.Unlock()

	l.links = append(l.links, link)
}

// Len returns the number of links.
func (l *Links) Len() int {
	l.mutex.Lock()
	defer l.mutex.Unlock()

	return len(l.links)
}

// Links returns the list of links.
func (l *Links) Links() []Link {
	l.mutex.Lock()
	defer l.mutex.Unlock()

	return l.links
}
