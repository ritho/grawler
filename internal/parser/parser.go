// Package parser implements a HTML parser to look for other links to fetch.
package parser

import (
	"net/url"
	"strings"

	"golang.org/x/net/html"

	"gitlab.com/ritho/grawler/internal/log"
	"gitlab.com/ritho/grawler/internal/models"
)

// Parser parses an HTML page and identifies inner html links to fetch.
type Parser interface {
	Parse(content string) (*models.Links, error)
}

type parser struct {
	baseURL string
	path    string
}

// New returns a new parser.
func New(baseURL, path string) Parser {
	u, _ := url.Parse(baseURL + path)
	return &parser{
		baseURL: u.String(),
		path:    path,
	}
}

// Parse the html content for new links to get.
func (p *parser) Parse(content string) (*models.Links, error) {
	links := models.NewLinks()
	reader := strings.NewReader(content)
	doc, err := html.Parse(reader)
	if err != nil {
		return nil, err
	}

	p.scanner(doc, links)

	return links, nil
}

func (p *parser) scanner(node *html.Node, links *models.Links) {
	if node.Type == html.ElementNode && node.Data == "a" {
		for i := range node.Attr {
			if node.Attr[i].Key != "href" {
				continue
			}

			log.Debugf("checking link %s against %s or %s\n", node.Attr[i].Val,
				p.baseURL, p.path)
			if p.shouldAddLink(node.Attr[i].Val) {
				value := strings.TrimPrefix(node.Attr[i].Val, p.baseURL)
				link, err := models.NewLink(value)
				if err == nil {
					links.Push(link)
				}
			}
		}
	}

	for c := node.FirstChild; c != nil; c = c.NextSibling {
		p.scanner(c, links)
	}
}

func (p *parser) shouldAddLink(link string) bool {
	if strings.HasPrefix(link, p.baseURL) {
		return true
	}

	if strings.HasPrefix(link, p.path) {
		return true
	}

	return false
}
