// Package storer implements the needed functionality to store the html content
// in the output directory.
package storer

import (
	"fmt"
	"os"
	"path"
	"strings"

	"gitlab.com/ritho/grawler/internal/log"
)

const (
	dirPermissions  = os.ModeDir | os.ModePerm
	filePermissions = os.FileMode(0o600)

	rootPath   = "/"
	indexPage  = "/index.html"
	htmlSuffix = ".html"
)

// Storer stores some content linked to a name.
type Storer interface {
	Store(name string, content string, force bool) error
	Exists(name string) bool
}

type storer struct {
	currentDir string
	baseDir    string
}

// New returns a new storer.
func New(dir string, resume bool) (Storer, error) {
	currentDir, err := os.Getwd()
	if err != nil {
		return nil, err
	}

	if !path.IsAbs(dir) {
		dir = path.Join(currentDir, dir)
	}

	st := &storer{
		currentDir: currentDir,
		baseDir:    dir,
	}

	if err := st.createDirectory(dir); err != nil {
		return nil, err
	}

	if !resume {
		log.Debugf("Clean execution, removing previous content.\n")
		if err := os.RemoveAll(dir); err != nil {
			return st, err
		}

		return st, os.Mkdir(dir, dirPermissions)
	}

	return st, nil
}

func (s *storer) Exists(name string) bool {
	if name == rootPath {
		name = indexPage
	}

	if !strings.HasSuffix(name, htmlSuffix) {
		name += htmlSuffix
	}

	filepath := path.Join(s.baseDir, name)
	log.Debugf("checking if %s exists\n", name)

	dst, err := os.Stat(filepath)
	if os.IsNotExist(err) {
		return false
	}

	if err != nil {
		fmt.Printf("error checking that %s exists: %s, assuming it does",
			filepath, err)
		return true
	}

	if dst.IsDir() {
		return false
	}

	return true
}

// Store the content linked to a name.
func (s *storer) Store(name string, content string, force bool) error {
	if name == rootPath {
		name = indexPage
	}

	if !strings.HasSuffix(name, htmlSuffix) {
		name += htmlSuffix
	}

	filepath := path.Join(s.baseDir, name)
	log.Debugf("storing content %s into %s\n", name, filepath)

	dir := path.Dir(filepath)
	if err := s.createDirectory(dir); err != nil {
		return err
	}

	if err := s.checkPath(filepath, force); err != nil {
		return err
	}

	return os.WriteFile(filepath, []byte(content), filePermissions)
}

func (s *storer) checkPath(filepath string, force bool) error {
	dst, err := os.Stat(filepath)
	if err != nil && !os.IsNotExist(err) {
		return err
	}

	if err == nil {
		if dst.IsDir() {
			return fmt.Errorf("%s is a directory", filepath)
		}

		if !force {
			return nil
		}

		return os.Remove(filepath)
	}

	return nil
}

func (s *storer) createDirectory(dir string) error {
	if s.directoryExists(dir) {
		return nil
	}

	dst, err := os.Stat(dir)
	if err != nil && !os.IsNotExist(err) {
		return err
	}

	if os.IsNotExist(err) || !dst.IsDir() {
		log.Debugf("creating directory %s\n", dir)

		return os.MkdirAll(dir, dirPermissions)
	}

	return nil
}

func (s *storer) directoryExists(dir string) bool {
	log.Debugf("checking if directory %s exists\n", dir)
	dst, err := os.Stat(dir)
	if os.IsNotExist(err) {
		return false
	}

	return dst.IsDir()
}
